import java.util.*;

public class Main {

    private static int[] clock;
    private static Scanner scanner;

    public static void main(String[] args) {

        scanner = new Scanner(System.in);

        //mozna zrobic rozne metody, albo random, albo podaj cyfry, albo podaj tablice, albo...

        int[] digit = new int[4];
        System.out.println("podaj 4 cyfry");
        digit[0] = scanner.nextInt();
        digit[1] = scanner.nextInt();
        digit[2] = scanner.nextInt();
        digit[3] = scanner.nextInt();

        if (checkYourDigit(digit)) {
            make4number(digit);
            minimumClock();
            maximumClock();
        } else {
            System.out.println("soorrry man...");
        }

    }

    private static boolean checkYourDigit(int[] digit) {

        Arrays.sort(digit);
        return ((digit[0] < 2 && digit[1] < 6) || (digit[0] == 2 && digit[1] < 4 && digit[2] < 6));
    }

    private static int[] make4number(int[] digit) {

        int counter = 0;
        clock = new int[24];
        for (int i = 0; i < digit.length; i++) {
            for (int j = 0; j < digit.length; j++) {
                for (int k = 0; k < digit.length; k++) {
                    for (int l = 0; l < digit.length; l++) {
                        if (i != j && i != k && i != l && j != k && j != l && k != l) {
                            int clocks = 1000 * digit[i] + 100 * digit[j] + 10 * digit[k] + digit[l];
                            clock[counter] = clocks;
                            counter++;
                        }
                    }
                }
            }
        }
        Arrays.sort(clock);
        return clock;
    }


    private static void minimumClock() {

        checkTime();
    }

    private static void maximumClock() {

        resorting();
        checkTime();
    }

    private static void resorting() {

        int[] temp = new int[clock.length];
        for (int i = 0; i < clock.length; i++) {
            temp[i] = clock[clock.length - 1 - i];
        }
        System.arraycopy(temp, 0, clock, 0, clock.length);
    }

    private static void checkTime() {

        int hr;
        int min;
        for (int aClock : clock) {
            hr = aClock / 100;
            min = aClock % 100;
            if (hr < 24 && min < 60) {
                System.out.println("twoja godzina to: " + hr + ":" + min);
                break;
            }
        }
    }
}
